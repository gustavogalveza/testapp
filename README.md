# TestApp

[Angular CLI](https://github.com/angular/angular-cli) version 8.3.25.
App realizada para optar a desarrollador Front-end de **Mining TAG**

## Run App

- `git clone https://bitbucket.org/gustavogalveza/testapp/`
- `cd testapp`
- `npm i`
- `ng serve -o`

Ejecutar en una linea:
`git clone https://bitbucket.org/gustavogalveza/testapp/`
`cd testapp`
`npm i`
`ng serve -o`
