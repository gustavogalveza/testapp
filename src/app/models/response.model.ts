export class Response {

    constructor(
        public success: boolean,
        public error?: any,
        public data?: any
    ) {}
}
