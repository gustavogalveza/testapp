import { Component, OnInit } from '@angular/core';
import { FirstViewService } from '../../services/first-view.service';
import { Response } from '../../models/response.model';

@Component({
  selector: 'app-first-view',
  templateUrl: './first-view.component.html',
  styleUrls: ['./first-view.component.scss']
})
export class FirstViewComponent implements OnInit {

  datos: Response;
  // la variable arr será para ordenarla y no tener conflictos con la var original
  arr: any;
  // variable del textbox ordenado
  sort: string;

  // mostrar o no el loader
  loader = true;

  constructor(private _first: FirstViewService) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this._first.get().subscribe((resp: Response) => {
      if (resp.success) {
        // Si todo sale bien se llama a un toast para informar
        this.appendToast('Datos Obtenidos!', 'Los datos se han obtenido satisfactoriamente');
      }
      this.datos = resp;
      // asignamos para no reemplazar
      this.arr = Object.assign([], resp.data);
      if (this.datos.success && this.datos.data && (this.datos.data.length > 0)) {
        // Si estan los datos proceso los datos para formar un modelo
        this.datos.data.forEach((e, index, array) => {
          this.datos.data[index] = {n: e, ocurrency: null, first: null, last: null};
          if (index === (array.length - 1)) {
            // al finalizar el loop proceso los datos
            this.getOcurrency();
          }
        });
      }
    });
  }

  getOcurrency() {
    this.datos.data.forEach((ele, index_1, array_1) => {

      // #### loop para calcular la ocurrencia, el first y last.
      for (let index_2 = 0; index_2 < this.datos.data.length; index_2++) {
        let d = this.datos.data[index_2];
        if (d.n === ele.n) {
          ele.ocurrency ++;
          ele.last = index_2;
          if (ele.first == null) {
            ele.first = index_2;
          }
        }
      }
      // #### fin for 2


      /* al finalizar el loop inicial llenamos la variable sort para mostrarla en el textbox */
      if (index_1 === (array_1.length - 1)) {
        // finalizamos el loader y informamos con un toast
        this.loader = false;
        this.appendToast('Procesado!', 'Los datos, ya han sido procesado');
        this.sort_asc(this.datos.data);
      }
    });
  }

  sort_asc(list) {

    // variables para realizar el sort
    let min;
    this.sort = '';
    // fin variables para sort


    this.arr.forEach((ele, index_1, array_1) => {

      // #### sort clasico al más puro estilo del lenguaje C ####
      min = index_1;
      for (let j = index_1 + 1; j < this.arr.length; j++) {
        // se recorre desde el valor siguiente hacía delante
        // buscando el valor minimo
        if (this.arr[j] < this.arr[min]) {
          min = j;
          // y se guarda el index en el valor min
        }
      }
      /*al recorrer todo se dan vuelta los valores dejando
        el valor minimo al inicio */
      let temp = this.arr[min];
      this.arr[min] = this.arr[index_1];
      this.arr[index_1] = temp;
      // #### fin sort ####
      if (index_1 === (array_1.length - 1)) {
        this.arr.forEach((e, index, array) => {
          if (index === (array.length - 1)) {
            this.sort += String(e);
          } else {
            this.sort += String(e) + ', ';
          }
        });
      }
    });
  }

  // función para crear un toast y adjuntarlo al div id="toast_container"
  appendToast(title: string, description: string, date: string = '') {
    let toast = document.createElement('div');
    toast.classList.add('toast');
    toast.setAttribute('data-delay', '4000');
    toast.setAttribute('role', 'alert');
    toast.setAttribute('aria-live', 'assertive');
    toast.setAttribute('aria-atomic', 'true');
    toast.innerHTML =
        `<div class="toast-header">
              <strong class="mr-auto">${title}</strong>
              <small class="text-muted">${date}</small>
              <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
        </div>
        <div class="toast-body">
            ${description}
        </div>`;
    $('#toast_container').append(toast);
    $(toast).toast('show');
  }

}
