import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styles: [`
  .page-wrapper {
    margin-top: 56px;
  }
  @media only screen and (min-width: 600px) {
    .page-wrapper {
      margin-left: 5rem;
    }
  }
  @media only screen and (max-width: 600px) {
    .page-wrapper {
      margin-left: 0rem;
    }
  }
  `]
})
export class PagesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
