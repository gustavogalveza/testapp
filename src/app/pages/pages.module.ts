import { NgModule, LOCALE_ID } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule, registerLocaleData } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { PagesComponent } from './pages.component';
import { PAGES_ROUTES } from './pages.routes';
import { FirstViewComponent } from './first-view/first-view.component';
import { SecondViewComponent } from './second-view/second-view.component';

@NgModule({
    declarations: [
        PagesComponent,
        FirstViewComponent,
        SecondViewComponent
    ],
    exports: [],
    imports: [
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        PAGES_ROUTES,
        CommonModule,
    ],
    providers: [
        { provide: LOCALE_ID, useValue: 'es-CL' }
    ]
})
export class PagesModule { }
