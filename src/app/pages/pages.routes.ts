import { RouterModule, Routes } from '@angular/router';

import { PagesComponent } from './pages.component';
import { FirstViewComponent } from './first-view/first-view.component';
import { SecondViewComponent } from './second-view/second-view.component';


const pagesRoutes: Routes = [
    {
        path: '', component: PagesComponent,
        children: [
            { path: '', redirectTo: '/first', pathMatch: 'full' },
            {path: 'first', component: FirstViewComponent},
            {path: 'second', component: SecondViewComponent}
        ]
    }
];


export const PAGES_ROUTES = RouterModule.forChild( pagesRoutes );
