import { Component, OnInit } from '@angular/core';
import { SecondViewService } from '../../services/second-view.service';
import { Response } from '../../models/response.model';

@Component({
  selector: 'app-second-view',
  templateUrl: './second-view.component.html',
  styleUrls: ['./second-view.component.scss']
})
export class SecondViewComponent implements OnInit {
  datos: Response;

  jsonData: any;

  loader = true;

  header = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'
  , 'ñ', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

  constructor(private _second: SecondViewService) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this._second.get().subscribe((resp: Response) => {
      if (resp.success) {
        // Si todo sale bien se llama a un toast para informar
        this.appendToast('Datos Obtenidos!', 'Los datos se han obtenido satisfactoriamente');
      }
      this.datos = resp;
      if (this.datos.success && this.datos.data) {
        // Si estan los datos obtenemos el json y paramos el loader
        this.jsonData = JSON.parse(this.datos.data);
        this.loader = false;
      }
    });
  }

  match(exp: string, paragraph: string) {
    try {
      /* se crea un listado con el caracter como match
      y la longitud del listado será igual menos uno a la cantidad
      de caracteres.
      # aunque no es la forma más optima es la unica que se me ocurrió*/
      return paragraph.toLocaleLowerCase().split(exp).length - 1;
    } catch (error) {
      return 'error';
    }
  }

  sum_number_paragraph(paragraph: string) {
    // se obtiene los numero del parrafo con un match y un regex
    let list = paragraph.match(/\d+/g);
    if (list) {
      let sum = 0;
      // se suma el listado y se retorna
      list.forEach(l => sum += Math.abs(Number(l)));
      return sum;
    }
  }


  // función para crear un toast y adjuntarlo al div id="toast_container"
  appendToast(title: string, description: string, date: string = '') {
    let toast = document.createElement('div');
    toast.classList.add('toast');
    toast.setAttribute('data-delay', '4000');
    toast.setAttribute('role', 'alert');
    toast.setAttribute('aria-live', 'assertive');
    toast.setAttribute('aria-atomic', 'true');
    toast.innerHTML =
        `<div class="toast-header">
              <strong class="mr-auto">${title}</strong>
              <small class="text-muted">${date}</small>
              <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
        </div>
        <div class="toast-body">
            ${description}
        </div>`;
    $('#toast_container').append(toast);
    $(toast).toast('show');
  }

}
