import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FirstViewService {

  constructor(private http: HttpClient) { }

  get() {
    let url = 'http://patovega.com/prueba_frontend/array.php';
    return this.http.get(url);
  }
}
