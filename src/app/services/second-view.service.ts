import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SecondViewService {

  constructor(private http: HttpClient) { }

  get() {
    let url = 'http://patovega.com/prueba_frontend/dict.php';
    return this.http.get(url);
  }
}
